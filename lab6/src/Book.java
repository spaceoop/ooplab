/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* Book.java: This is my Book!
**********************************************/

public class Book{

	// private fields
	private int bookID;
	private String bookName;
	private boolean isAvailable;
	public static int totalNoOfBook = 0;
	public static int totNoOfAvailableBook = 0;

	// public constructor
	public Book(int id, String name){
		bookID = id;
		bookName = name;
		isAvailable = true;
		totalNoOfBook++;
		totNoOfAvailableBook++;
	}

	public Book(String name){
		this(totalNoOfBook, name);
	}

	public void borrow(LibraryCard libCard){
		libCard.incrementBorrowedNum();
		isAvailable = false;
		totNoOfAvailableBook--;
		//add some comment
	}

	public int getBookID(){
		return bookID;
	}

	// print available book info
	public void printAvailableBookInfo(){
		if(isAvailable){
			System.out.println("------------------------------------------");
			System.out.println("                 Book                     ");
			System.out.println("------------------------------------------");
			System.out.println("Book ID:   " + bookID);
			System.out.println("Book Name: " + bookName);
			System.out.println("------------------------------------------");
		}
	}
}
