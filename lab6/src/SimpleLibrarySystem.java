/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* SimpleLibrarySystem.java: My Library System!
**********************************************/

import java.util.Scanner;
import java.util.ArrayList;
import javax.swing.*;

public class SimpleLibrarySystem{
	public static final int TOTALBOOK = 10;

	public static void main(String[] args){
		// load library books
		Book[] books = new Book[3];
		books[0] = new Book("Java");
		books[1] = new Book("Database");
		books[2] = new Book("Networking");

		ArrayList<Book> bookList = new ArrayList<Book>();

		for(int i=0;i<TOTALBOOK;i++){
			String bookName = JOptionPane.showInputDialog(null, "Book name for item " + i);
			bookList.add(new Book(i, bookName));
		}

		// load libarary card
		LibraryCard libcard = new LibraryCard("guest", "1234");

		System.out.println("**********************");
		System.out.println("Welcome to OOP library");
		System.out.println("**********************");
		System.out.println();

		Scanner scanner = new Scanner(System.in);

		System.out.print("Please enter your user name: ");
		String username = scanner.next();

		System.out.print("Please enter your password: ");
		String password = scanner.next();

		System.out.println();

		if(libcard.validateCard(username, password)){
			System.out.println("Login sucessfully!");
			System.out.println();

			while(true){
				System.out.println("List of avilable books:");
				for(Book thisBook: bookList)
					thisBook.printAvailableBookInfo();

				System.out.print("Would you like to borrow a book (y/n)? ");
				String option = scanner.next();

				if(option.equals("y")){
					System.out.print("Please enter the book ID: ");
					int borrowID = scanner.nextInt();
					// books[borrowID].borrow(libcard);
					for(Book thisBook: bookList) if(thisBook.getBookID() == borrowID){
						thisBook.borrow(libcard);
					}
				}else{
					break;
				}
			}

			libcard.printLibraryCard();
		}else{
			System.out.println("Login fail! :C");
		}

		System.out.println("Bye!");
		scanner.close();
	}
}
