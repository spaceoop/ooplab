/*********************************************
* Course: OOP
*
* Sample code, for reference
 * Function: Represent the class of library card
 **********************************************/

import java.util.Calendar;
import java.util.Date;

class LibraryCard {
	// Fields / Data members
	private String userName;
	private String cryptPassword;
	private Calendar dateOfApplication;
	private int borrowedNum;		// 0: available, 1 or above: borrowed and number of reserved advanced
	
	// Constructor
	public LibraryCard(String name, String password) {
		// initialization
		userName = name;
		cryptPassword = CryptWithMD5.cryptWithMD5(password);
		borrowedNum = 0;
		dateOfApplication = Calendar.getInstance();
	}

	public void incrementBorrowedNum ( ){borrowedNum++;}
	
	// Validate user name and password
	public boolean validateCard(String userName, String password){
		if(userName.equals(this.userName) && (CryptWithMD5.cryptWithMD5(password)).equals(this.cryptPassword))
			return true;
		else
			return false;
	}

	
	// Print the library information
	public void printLibraryCard() {
		System.out.println("----------------------------------------------------------");
		System.out.println("                     OOP Library Card                     ");
		System.out.println("----------------------------------------------------------");
		System.out.println("User name \t\t: " + userName);
		System.out.println("Crypt Password \t\t: " + cryptPassword);
		System.out.println("Borrowed items \t\t: " + borrowedNum);
		System.out.println("Date of Application \t: " + dateOfApplication.getTime());
		System.out.println("----------------------------------------------------------");
	}
	
	
}