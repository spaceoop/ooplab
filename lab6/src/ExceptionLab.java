/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* ExceptionLab.java: Exception handling!
**********************************************/

//import statements
import java.util.Scanner;
import java.util.InputMismatchException;

public class ExceptionLab {
	public static void main(String[] args) {
		int[] intNumbers = new int[1]; //s4
		int result = 0;
		Scanner scanner = new Scanner(System.in);

		try{
			for (int i=0; i<intNumbers.length; i++){
				System.out.print("Integer " + (i+1) + " : ");
				intNumbers[i] = scanner.nextInt();//s3
			}
			result = intNumbers[0] / intNumbers[1]; //s2, s4
			System.out.println("Result = " + result);
		}catch(ArithmeticException e){
			System.err.println("Divided by zero! :C");
		}catch(InputMismatchException e){
			System.err.println("You made stupid input! :C");
		}catch(ArrayIndexOutOfBoundsException e){
			System.out.println("WHY U OUT OF BOUND! :C");
		}
		scanner.close();
	}
}
