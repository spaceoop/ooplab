/*********************************************
* Course: OOP
* 
* Given file for lab ex, reference only
**********************************************/

import java.util.Calendar;
import java.util.Date;

class LibraryCard {

	// Declare data members
	private int cardID;
	private String userName;
	private Calendar dateOfApplication;
	private int borrowedNum;
	
	// Constructor
	public LibraryCard(String name, int id) {
		// initialization
		cardID = id;
		userName = name;
		borrowedNum = 0;
		dateOfApplication = Calendar.getInstance();
	}

	// Print the library information
	public void printLibraryCard() {
		System.out.println("----------------------------------------------------------");
		System.out.println("                     Library Card                         ");
		System.out.println("----------------------------------------------------------");
		System.out.println("Card ID \t\t: " + cardID);
		System.out.println("Name \t\t\t: " + userName);
		System.out.println("Borrowed items \t\t: " + borrowedNum);
		System.out.println("Date of Application \t: " + dateOfApplication.getTime());
		System.out.println("----------------------------------------------------------");
	}
	
	// Return the value of cardID
	public int getCardID() {
		return cardID;
	}

	// Add a method to compare if id and user name match the current library card
	public boolean compareLibraryCard(int id, String userName) {
		if(id == cardID && this.userName.equals(userName))
			return true;
		else
			return false;
	}

	// Update the borrowed number
	public void incrementBorrowedNum( ) {
		borrowedNum++;
	}
	
}