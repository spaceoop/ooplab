/*********************************************
* Course: OOP
*
* Given file for lab ex, reference only
**********************************************/

import java.util.ArrayList;

public class BookRecordSystem {

	public static final String DEF_BOOKFILENAME = "books.csv";
	public static void main(String[] args) {
		// TESTING Code below: for checking class data member
		System.out.println("SYSTEM START: Total number of books = " + Book.totalNoOfBook);
		// creating four books (3 LibraryBook and 1 Book) in an ArrayList HERE
		ArrayList<Book> bookList = new ArrayList<Book>();

		bookList.add(new LibraryBook("Lord of the Flies",
						"William Golding", 2013));
		bookList.add(new Book(4023, "Who Loves OOP?"));
		bookList.add(new LibraryBook(123, "Animal Farm", "George Orwell", 1996));
		bookList.add(new Book("A Book's Life"));
		// ... and more HERE

		//Show book info
		for(int i = 0 ; i < bookList.size(); i++) {
			bookList.get(i).printBookInfo();
		}

		// TESTING Code below: for checking File I/0
		System.out.println("/n////////////////// File I/O testing Section /////////////////");
		BookIO.writeLBTxtFile(DEF_BOOKFILENAME, bookList);
		System.out.println("Write OK: " + DEF_BOOKFILENAME);
		ArrayList<Book> bookListIn = BookIO.readLBTxtFile(DEF_BOOKFILENAME);
		System.out.println("Read OK: " + DEF_BOOKFILENAME);

		//Show book info
		for(int i = 0 ; i < bookListIn.size(); i++) {
			bookListIn.get(i).printBookInfo();
		}

		// TESTING Code below: for checking class data member
		System.out.println("SYSTEM END: Total book objects created = " + Book.totalNoOfBook);
		System.out.println("End of program.");
	}
}
