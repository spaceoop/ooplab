/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* LibraryBook.java: This is my Book from the library!
**********************************************/

public class LibraryBook extends Book{
	public static int totalNoOfLibraryBook = 0;
	private String author;
	private int year;

	public LibraryBook(String name, String author, int year){
		super(name);
		this.author = author;
		this.year = year;
	}

	public LibraryBook(int bID, String name, String author, int year){
		super(bID, name);
		this.author = author;
		this.year = year;
	}

	public String getAuthor(){
		return author;
	}

	public int getYear(){
		return year;
	}

	@Override
	public void printBookInfo() {
		String statusStr;
		if(isAvailable) statusStr = "Available";
		else statusStr = "NOT Available";
		System.out.println("----------------------------------------------------------");
		System.out.println("                         Book (" + statusStr + ")");
		System.out.println("----------------------------------------------------------");
		System.out.println("Book ID\t\t: " + bookID);
		System.out.println("Book name\t: " + bookName);
		System.out.println("Author\t\t: " + author);
		System.out.println("Year\t\t: " + year);
		System.out.println("----------------------------------------------------------");
	}

}
