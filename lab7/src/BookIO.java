/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* BookIO.java: CSV File IO!
**********************************************/

import java.util.*;
import java.io.*;

public class BookIO {
	public static ArrayList<Book> readLBTxtFile(String inFileStr){
		ArrayList<Book> rt = new ArrayList<Book>();

		try{
			File inFile = new File(inFileStr);
			Scanner sc = new Scanner(inFile);
			String header = sc.nextLine();
			while(sc.hasNextLine()){
				String[] dat = sc.nextLine().split(",");
				int id = Integer.parseInt(dat[0]);
				String name = dat[1];
				if(dat.length < 4){
					rt.add(new Book(id, name));
				}else{
					String author = dat[2];
					int year = Integer.parseInt(dat[3]);
					rt.add(new LibraryBook(id, name, author, year));
				}
			}
			sc.close();
		}catch(FileNotFoundException e){
			System.err.println("File not found! :C");
		}

		return rt;
	}

	public static boolean writeLBTxtFile(String outFileStr, ArrayList<Book> bList){
		try{
			File outFile = new File(outFileStr);
			PrintWriter pw = new PrintWriter(outFile);
			pw.println("Book ID, Book name, Author(Only for Library Book), Year(Only for Library Book)");
			for(Book b: bList){
				if(b instanceof LibraryBook){
					LibraryBook lb = (LibraryBook) b;
					pw.println(lb.bookID + "," + lb.bookName + "," + lb.getAuthor() + "," + lb.getYear());
				}else{
					pw.println(b.bookID + "," + b.bookName);
				}
			}
			pw.close();
		}catch(FileNotFoundException e){
			System.err.println("File not found! :C");
			return false;
		}
		return true;
	}
}
