/*********************************************
* Course: OOP
*
* Given file for lab ex, reference only
**********************************************/

class Book{

// Declare data members //////////////////////////
	public static int totalNoOfBook = 0;  // class data member
	public static int totalNoOfAvailableBook = 0;  // class data member

	protected int bookID;
	protected String bookName;
	protected boolean isAvailable = true;
	private int borrowerID = -1;	// -1 indicates the book is available (not borrowed)

// Constructors //////////////////////////
	Book( ) {
	// initialization of all books
		totalNoOfBook++;
		totalNoOfAvailableBook++;
	}

	public Book(String name) {	// only book name, with book ID assigned in a simple way
		this();
		bookID = totalNoOfBook; // initial bookID assigned in a simple way
		bookName = name;
	}

	public Book(int bID, String name) {	// with book ID specified.
		this(name);
		bookID = bID;  // bookID re-assigned with the input para
	}

// Methods //////////////////////////
	// Borrow a book
	public boolean borrowBook(LibraryCard card) {
		if (card==null) return false;
		isAvailable = false;
		borrowerID = card.getCardID();
		totalNoOfAvailableBook--;
		return true;
	}

	// Return a book
	public boolean returnBook(LibraryCard card) {
		if (isAvailable) return false;
		isAvailable = true;
		borrowerID = -1;
		totalNoOfAvailableBook++;
		return true;
	}

	// Print the book information
	public void printBookInfo() {
		String statusStr;
		if(isAvailable) statusStr = "Available";
		else statusStr = "NOT Available";
		System.out.println("----------------------------------------------------------");
		System.out.println("                         Book (" + statusStr + ")");
		System.out.println("----------------------------------------------------------");
		System.out.println("Book ID\t\t: " + bookID);
		System.out.println("Book name\t: " + bookName);
		System.out.println("----------------------------------------------------------");
	}

}
