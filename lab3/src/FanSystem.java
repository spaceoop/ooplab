/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* FanSystem.java: To develop an awesome fan system :D
**********************************************/

class FanSystem{

	public static void main(String[] args){
		Fan first = new Fan();
		Fan second = new Fan();
		Fan third = new Fan();

		first.setSpeed(2);
		second.setSpeed(10);
		// third.noNeedSet();

		System.out.println("Fan 1: " + first.getMessage());
		System.out.println("Fan 2: " + second.getMessage());
		System.out.println("Fan 3: " + third.getMessage());

		System.out.println("\n>>> Program developed by Li Kwan To (20027290) <<<");
	}
}
