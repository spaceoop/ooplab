/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* Fan.java: My Fan implementation!
**********************************************/

class Fan{

	int speed;
	boolean on;

	public Fan(){
		speed = 0;
		on = false;
	}

	public String getMessage(){
		if(on)
			return "Fan is ON (speed = " + speed + ")";
		else
			return "Fan is OFF!";
	}

	public void setSpeed(int newSpeed){
		newSpeed = Math.max(newSpeed, 0);
		newSpeed = Math.min(newSpeed, 5);
		if(newSpeed > 0){
			speed = newSpeed;
			on = true;
		}else{
			speed = 0;
			on = false;
		}
	}
}
