/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* FanSystemExtra.java: To develop an awesome fan system :D
**********************************************/

import javax.swing.*;
class FanSystemExtra{

	public static void main(String[] args){

		for(int i=0;i<5;i++){
			Fan myFan = new Fan();
			String strInput = JOptionPane.showInputDialog(null, "Enter the speed of Fan " + (i + 1) + "!");
			int speed = Integer.parseInt(strInput);
			myFan.setSpeed(speed);
			System.out.println("Fan " + (i + 1) + ": " + myFan.getMessage());
		}

		System.out.println("\n>>> Program developed by Li Kwan To (20027290) <<<");
	}
}
