/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* MainTestRunner.java: Run main test!
**********************************************/

public class MainTestRunner{
	public static void main(String[] args){
		Runner dummy = new Runner("Dummy");
		Runner au = new Runner("AU");

		dummy = new Runner("Dummy");
		Runner10K dick = new Runner10K("Dick", 1800);

		au.dispRunnerInfo();

		dummy = new Runner("Dummy");
		dick.dispRunnerInfo();
	}
}
