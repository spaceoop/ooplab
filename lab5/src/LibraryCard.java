/*********************************************
* Course: OOP
*
* Given file for lab ex, reference only
**********************************************/

import java.util.Calendar;
import java.util.Date;

class LibraryCard {

	// Declare data members
	private String userName;
	private String cryptPassword;
	private Calendar dateOfApplication;
	private int borrowedNum;

	// Constructor
	public LibraryCard(String name, String password) {
		// initialization
		userName = name;
		cryptPassword = CryptWithMD5.cryptWithMD5(password);
		borrowedNum = 0;
		dateOfApplication = Calendar.getInstance();
	}

	public boolean validateCard(String username, String password){
		return username.equals(userName) && cryptPassword.equals(CryptWithMD5.cryptWithMD5(password));
	}

	public void incrementBorrowedNum(){
		borrowedNum++;
	}

	// Print the library information
	public void printLibraryCard() {
		System.out.println("----------------------------------------------------------");
		System.out.println("                     OOP Library Card                     ");
		System.out.println("----------------------------------------------------------");
		System.out.println("User name \t\t: " + userName);
		System.out.println("Crypt Password \t\t: " + cryptPassword);
		System.out.println("Borrowed items \t\t: " + borrowedNum);
		System.out.println("Date of Application \t: " + dateOfApplication.getTime());
		System.out.println("----------------------------------------------------------");
	}


}
