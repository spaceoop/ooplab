/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* Runner10K.java: This is my Runner10K!
**********************************************/

public class Runner10K extends Runner{
	private int pBT;

	Runner10K(String name, int bestTime){
		super(name);
		pBT = bestTime;
	}

	@Override
	public void dispRunnerInfo(){
		super.dispRunnerInfo();
		System.out.println("The best time of this 10K runner is [" + pBT + "] seconds.");
	}
}
