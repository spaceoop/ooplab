/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* Runner.java: This is my Runner!
**********************************************/

public class Runner{
	private String rName;
	public static int totNumOfRunner = 0;

	public Runner(String name){
		rName = name;
		totNumOfRunner++;
	}

	public void dispRunnerInfo(){
		System.out.print("There are [" + totNumOfRunner + "] runners,");
		System.out.println(" and this runner's name is [" + rName + "].");
	}

}
