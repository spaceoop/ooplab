/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* Lab21PDType.java: To test primitive data types
**********************************************/

class Lab21PDType{

	public static void main(String[] args){

		// Step 5
		System.out.println("Checking of Primitive Data Type.");

		// Steps 6, 7
		int x = -32770;
		double y = 35.5;
		double z = 50;
		System.out.println("x = " + x);
		System.out.println("y = " + y);
		System.out.println("z = " + z);

		// Step 8
		double sum = x + y + z;
		System.out.println("SUM = " + sum);

		// Step 9
		final int MONTH_IN_YEAR = 12;
		// MONTH_IN_YEAR = 13;

		// Step 10
		double monthly = sum / MONTH_IN_YEAR;
		System.out.println("monthly = " + monthly);

		// Step 11
		double myMax = Math.max(Math.max(x, y), z);
		System.out.println("myMax = " + myMax);

		double mySqrt = Math.sqrt(myMax);
		System.out.println("mySqrt = " + mySqrt);

		// Step 12
		x = (short) y;
		System.out.println("newX = " + x);

		// Steo 13
		System.out.println("Monthly-mean temperature in Hong Kong: ");
		double[] myMean = {16.3, 16.8, 19.1, 22.6, 25.9, 27.9, 28.8, 28.6, 27.7, 25.5, 21.8, 17.9};
		for(double meanX: myMean)
			System.out.print(meanX + " ");
		System.out.println();

	}

}
