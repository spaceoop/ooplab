/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* Lab22HiManyGUI.java: To open many windows :P
**********************************************/

import javax.swing.*;
import java.awt.Color;

class Lab22HiManyGUI{

	public static void main(String[] args){
		JFrame[] myWindowArray = new JFrame[12];
		for(int i=0;i<myWindowArray.length;i++){
			myWindowArray[i] = new JFrame("This is my number " + (i + 1) + " window!");
			myWindowArray[i].setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			myWindowArray[i].setSize(500, 100 + (int) (Math.random() * 400));
			myWindowArray[i].getContentPane().setBackground(Math.random() > 0.4 ? Color.RED : Color.GREEN);
			myWindowArray[i].setVisible(true);
		}
	}

}
