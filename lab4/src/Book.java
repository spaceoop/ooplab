/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* Book.java: This is my Book!
**********************************************/

public class Book{

	// private fields
	private int bookID;
	private String bookName;
	private boolean isAvailable;

	// public constructor
	public Book(int id, String name){
		bookID = id;
		bookName = name;
	}

	// public method
	public void setBorrow(boolean tf){
		isAvailable = tf;
	}

	// override default toString()
	@Override
	public String toString(){
		return "BookID: " + bookID + ", BookName: " + bookName + ", isAvailable: " + isAvailable;
	}
}
