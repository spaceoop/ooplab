/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* LibrarySystem.java: My Library System!
**********************************************/

public class LibrarySystem{
	public static void main(String[] args){
		Book book1 = new Book(1, "Lord of the Files");
		Book book2 = new Book(2, "Who Loves OOP?");
		Book book3 = new Book(3, "Animal Farm");

		book1.setBorrow(true);
		book2.setBorrow(false);
		book3.setBorrow(false);

		System.out.println(book1);
		System.out.println(book2);
		System.out.println(book3);

	}
}
