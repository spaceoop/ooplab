/*********************************************
* Course: OOP
* 
* Given file for lab ex, reference only
**********************************************/

import java.util.Scanner;

public class LibraryCardGeneration {

	public static void main(String[] args) {

		String userName;
		String password;
		
		System.out.println("************************");
		System.out.println(" Welcome to OOP Library");
		System.out.println("************************");
		
		Scanner scanner = new Scanner(System.in);
		
		
		// Read user name from standard input (Task: user name cannot be empty)
		System.out.print("Please set your user name: ");
		userName = scanner.nextLine();
				
		
		// Read user password from standard input (Task: Ask the user to re-enter the password to reconfirm)
		System.out.print("Please set your password: ");
		password = scanner.nextLine();
			
		
		// Create a new library card
		LibraryCard newCard = new LibraryCard(userName, password);

		System.out.println("Your library card is generated successfully!");

		newCard.printLibraryCard();
		
		scanner.close();
	}
}
