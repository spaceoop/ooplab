/*********************************************
* Course: OOP
* Given file for lab ex, reference only
* 
* Students may ignore the detailed theory behind this encryption approach
* However, students should be able to use this encryption by properly calling the method
*
* Main Purpose: Encryption: input a string, and return the corresponding encrypted string
* 
* Sample Usage: String encryptedStr = CryptWithMD5.cryptWithMD5(orgString);
*
**********************************************/

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CryptWithMD5 {
   private static MessageDigest md;

   public static String cryptWithMD5(String pass){
    try {
        md = MessageDigest.getInstance("MD5");
        byte[] passBytes = pass.getBytes();
        md.reset();
        byte[] digested = md.digest(passBytes);
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<digested.length;i++){
            sb.append(Integer.toHexString(0xff & digested[i]));
        }
        return sb.toString();
    } catch (NoSuchAlgorithmException ex) {
        Logger.getLogger(CryptWithMD5.class.getName()).log(Level.SEVERE, null, ex);
    }
        return null;
   }
}