/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* LibraryCard.java: This is my Library Card!
**********************************************/

import java.util.Calendar;
import java.text.SimpleDateFormat;

public class LibraryCard{
	// private fields
	private String userName;
	private String cryptPassword;
	private Calendar dateOfApplication;
	private int borrowedNum;

	// public constructor
	public LibraryCard(String name, String password){
		userName = name;
		cryptPassword = CryptWithMD5.cryptWithMD5(password);
		dateOfApplication = Calendar.getInstance();
		borrowedNum = 0;
	}

	// methods
	public void printLibraryCard(){
		System.out.println("========================================");
		System.out.println("           OOP Library Card             ");
		System.out.println("========================================");
		System.out.println("User name           : " + userName);
		System.out.println("Crypt Password      : " + cryptPassword);
		System.out.println("Borrowed items      : " + borrowedNum);
		SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println("Date of Application : " + myFormat.format(dateOfApplication.getTime()));
		System.out.println("========================================");
	}

}
