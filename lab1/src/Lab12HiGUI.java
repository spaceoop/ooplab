/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* Lab12HiGUI.java: To show a green window on the screen
**********************************************/

import javax.swing.*;
import java.awt.Color;
class Lab12HiGUI{
	public static void main(String[] args){
		JFrame myWindow = new JFrame();
		myWindow.setTitle("Li Kwan To");
		myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myWindow.setSize(500, 500);
		myWindow.getContentPane().setBackground(Color.GREEN);
		myWindow.setVisible(true);
	}
}
