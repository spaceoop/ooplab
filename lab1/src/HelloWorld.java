/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* HelloWorld.java: To print "Hello World" on the screen
**********************************************/

class HelloWorld{
	public static void main(String[] args){
		System.out.println("Hello\nWorld!");
		System.out.println("\n>>> Program developed by Li Kwan To (20027290) <<<");
	}
}
