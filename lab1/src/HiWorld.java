/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* HiWorld.java: To say hi to the user :)
**********************************************/

import javax.swing.JOptionPane;
class HiWorld{

	private String myMessage;

	HiWorld(String message){
		myMessage = message;
	}

	void sayHi(){
		JOptionPane.showMessageDialog(null, "Hello " + myMessage + "! :)");
	}

	public static void main(String[] args){
		String nameStr = JOptionPane.showInputDialog(null, "What's your name?");
		HiWorld myWorld = new HiWorld(nameStr);
		myWorld.sayHi();
	}
}
