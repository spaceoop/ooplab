/**********************************************
* Student: Li Kwan To (20027290), AENG-CS
* Course: OOP 2017 Semester 1
*
* FirstGUIFrame.java: My FIRST (not really) GUI! <3
**********************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FirstGUIFrame extends JFrame implements ActionListener{

	JTextField jtf;
	JButton jb;

	public FirstGUIFrame(){
		super("Our first GUI");
		setSize(300, 200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		getContentPane().setLayout(new FlowLayout());

		jtf = new JTextField(20);
		getContentPane().add(jtf);

		jb = new JButton("OK");
		getContentPane().add(jb);

		jb.addActionListener(this);

		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e){
		if (e.getSource() == jb) {
			JOptionPane.showMessageDialog(null, "Text in TF [" + jtf.getText() + "]",
			"Button Pressed", JOptionPane.PLAIN_MESSAGE, new ImageIcon("logo.jpg"));
		}
	}

	public static void main(String[] args){
		new FirstGUIFrame();
	}

}
